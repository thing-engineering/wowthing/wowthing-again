import type { Dictionary } from '@/types'

const raiderIoScores: Dictionary<string> = {
    all: 'All',
    dps: 'DPS',
    healer: 'Healer',
    tank: 'Tank',
}

export { raiderIoScores }
