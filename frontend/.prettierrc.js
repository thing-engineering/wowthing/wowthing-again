module.exports = {
    semi: false,
    singleQuote: true,
    tabWidth: 4,

    svelteSortOrder: 'options-scripts-styles-markup',
}
