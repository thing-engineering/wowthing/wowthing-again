export interface Dictionary<TValue> {
    [K: string]: TValue
}
