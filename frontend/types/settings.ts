export interface Settings {
    general: {
        showClassIcon: boolean
        showItemLevel: boolean
        showRaceIcon: boolean
        showRealm: boolean
        showSpecIcon: boolean
    }

    home: {
        showCovenant: boolean
        showKeystone: boolean
        showMountSpeed: boolean
        showStatuses: boolean
        showVaultMythicPlus: boolean
        showVaultPvp: boolean
        showVaultRaid: boolean
        showWeeklyAnima: boolean
        showWeeklySouls: boolean
    }

    privacy: {
        anonymized: boolean
        public: boolean
        showInLeaderboards: boolean
    }
}
