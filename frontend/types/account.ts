export interface Account {
    id: number
    name: string
    tag: string
    enabled: boolean
}
