﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models
{
    public class BattleNetOptions
    {
        public string ClientID { get; set; }
        public string ClientSecret { get; set; }
    }
}
