﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models.API.Character
{
    public class ApiCharacterQuestsCompleted
    {
        public List<ApiObnoxiousObject> Quests { get; set; }
    }
}
