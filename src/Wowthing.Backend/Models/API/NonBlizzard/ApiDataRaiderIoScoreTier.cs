﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wowthing.Backend.Models.API.NonBlizzard
{
    public class ApiDataRaiderIoScoreTier
    {
        public int Score { get; set; }
        public string RgbHex { get; set; }
    }
}
