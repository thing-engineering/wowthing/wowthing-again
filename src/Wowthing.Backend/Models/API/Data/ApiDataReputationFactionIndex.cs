﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models.API.Data
{
    public class ApiDataReputationFactionIndex
    {
        public List<ApiObnoxiousObject> Factions { get; set; }
    }
}
