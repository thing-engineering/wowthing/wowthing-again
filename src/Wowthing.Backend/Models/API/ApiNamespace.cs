﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models.API
{
    public enum ApiNamespace
    {
        Static,
        Dynamic,
        Profile,
    }
}
