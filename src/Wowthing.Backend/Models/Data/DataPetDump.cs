﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models.Data
{
    public class DataPetDump
    {
        public int ID { get; set; }
        public int CreatureID { get; set; }
        public int Flags { get; set; }
    }
}
