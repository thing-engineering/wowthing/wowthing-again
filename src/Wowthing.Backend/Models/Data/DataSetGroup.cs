﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Backend.Models.Data
{
    public class DataSetGroup
    {
        public string Name { get; set; }
        public List<string> Things { get; set; }
    }
}
