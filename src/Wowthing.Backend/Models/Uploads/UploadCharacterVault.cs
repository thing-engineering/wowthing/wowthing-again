﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wowthing.Backend.Models.Uploads
{
    public class UploadCharacterVault
    {
        public int Level { get; set; }
        public int Progress { get; set; }
        public int Threshold { get; set; }
    }
}
