﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wowthing.Backend.Models.Uploads
{
    public class UploadCharacterMythicDungeon
    {
        public int Level { get; set; }
        public int Map { get; set; }
    }
}
