﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Lib.Enums
{
    public enum WowQuality
    {
        Poor,
        Common,
        Uncommon,
        Rare,
        Epic,
        Legendary,
        Artifact,
        Heirloom,
        WowToken,
    }
}
