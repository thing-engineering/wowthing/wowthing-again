﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Lib.Enums
{
    public enum WowFaction
    {
        Alliance,
        Horde,
        Neutral,
    }
}
