﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wowthing.Lib.Enums
{
    public enum WowMountSkill
    {
        None = 0,
        Apprentice = 1,
        Journeyman = 2,
        Expert = 3,
        Artisan = 4, // deprecated but still exists
        Master = 5,
    }
}
