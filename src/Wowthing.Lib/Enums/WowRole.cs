﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Lib.Enums
{
    public enum WowRole
    {
        None,
        Tank,
        MeleeDps,
        RangedDps,
        Healer,
    }
}
