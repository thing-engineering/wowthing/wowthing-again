﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wowthing.Lib.Models
{
    public class WowTitle
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string TitleFemale { get; set; }
        public string TitleMale { get; set; }
    }
}
